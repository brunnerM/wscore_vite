export type Article = {
  artNo: number
  artName: string
  artType: number
  artActive: number
  artInformation: any[]
}
