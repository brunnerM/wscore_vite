import type { Price } from "@/types/price"
import http from "@/api/http.api"

const api_route_prefix = "prices/"

export const PricesApi = {
  getAll() {
    return http.get<Price[]>(api_route_prefix)
  },
  get(id: number) {
    return http.get<Price>(api_route_prefix + id + "/")
  },
  create(data: Price) {
    return http.post<Price>(api_route_prefix, data)
  },
  update(id: number, data: Price) {
    // INFO: Java
    // muss hier ein patch machen statt einem put, da sonst der gesamte artikel erwartet wird
    // wenn z.B: prcPrice null ist gibt es einen Fehler zurück => Cannot update this entity because it does not exist yet.
    return http.put<Price>(api_route_prefix + id + "/", data)
  },
  delete(id: number) {
    return http.delete(api_route_prefix + id + "/")
  },
  deleteAll() {
    return http.delete(api_route_prefix)
  },
  findBy(key: string, value: string) {
    return http.get<Price[]>(`${api_route_prefix}?${key}=${value}`)
  },
}
