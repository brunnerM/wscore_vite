import { ref } from "vue"
import { PricesApi } from "@/api/prices.api"
import type { Price } from "@/types/price"

// by convention, composable function names start with "use"
export function usePrices() {
  const pricesLoading = ref<boolean>(false)
  const priceSaving = ref<boolean>(false)
  const prices = ref<Price[]>([])

  function loadAllPrices(): void {
    pricesLoading.value = true
    PricesApi.getAll().then(({ data }) => {
      prices.value = data
      pricesLoading.value = false
    })
  }

  // expose managed state as return value
  return { pricesLoading, prices, loadAllPrices, priceSaving }
}
