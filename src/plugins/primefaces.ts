import PrimeVue from "primevue/config"
import Button from "primevue/button"
import DataTable from "primevue/datatable"
import Column from "primevue/column"
import ContextMenu from "primevue/contextmenu"
import Dialog from "primevue/dialog"
import InputText from "primevue/inputtext"
import InputNumber from "primevue/inputnumber"
import SelectButton from "primevue/selectbutton"
import Dropdown from "primevue/dropdown"
import TabView from "primevue/tabview"
import TabPanel from "primevue/tabpanel"
import Editor from "primevue/editor"
import Tooltip from "primevue/tooltip"
import ToastService from "primevue/toastservice"
import Toast from "primevue/toast"
import Menubar from "primevue/menubar"
import Accordion from "primevue/accordion"
import AccordionTab from "primevue/accordiontab"
import Splitter from "primevue/splitter"
import SplitterPanel from "primevue/splitterpanel"
import Calendar from "primevue/calendar"
import ScrollPanel from "primevue/scrollpanel"
import Card from "primevue/card"
import Panel from "primevue/panel"
import Fieldset from "primevue/fieldset"
import Carousel from "primevue/carousel"
import Chart from "primevue/chart"

import "primevue/resources/themes/vela-blue/theme.css" //theme
import "primevue/resources/primevue.min.css" //core css
import "primeicons/primeicons.css" //icons
import "primeflex/primeflex.css"
import type { App } from "vue"

export default {
  install(app: App, options: object) {
    app.use(PrimeVue)
    app.use(ToastService)
    app.directive("tooltip", Tooltip)
    app.component("PToast", Toast)
    app.component("PAccordion", Accordion)
    app.component("PAccordionTab", AccordionTab)
    app.component("PSplitter", Splitter)
    app.component("PSplitterPanel", SplitterPanel)
    app.component("PDataTable", DataTable)
    app.component("PColumn", Column)
    app.component("PContextMenu", ContextMenu)
    app.component("PDialog", Dialog)
    app.component("PInputText", InputText)
    app.component("PInputNumber", InputNumber)
    app.component("PDropdown", Dropdown)
    app.component("PSelectButton", SelectButton)
    app.component("PTabView", TabView)
    app.component("PTabPanel", TabPanel)
    app.component("PEditor", Editor)
    app.component("PButton", Button)
    app.component("PMenubar", Menubar)
    app.component("PCalendar", Calendar)
    app.component("PScrollPanel", ScrollPanel)
    app.component("PCard", Card)
    app.component("PPanel", Panel)
    app.component("PFieldset", Fieldset)
    app.component("PCarousel", Carousel)
    app.component("PChart", Chart)
  },
}
