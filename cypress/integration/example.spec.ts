// https://docs.cypress.io/api/introduction/api.html

describe("My First Test", () => {
  it("visits the app root url", () => {
    cy.visit("/")
    cy.contains("h1", "Startseite")
  })
  it("navigate to article view", () => {
    cy.visit("/")
    cy.contains("Tabellen").click()
    cy.contains("Artikel").click()
  })
  it("displays test articles", () => {
    cy.intercept(
      {
        method: "GET", // Route all GET requests
        url: "/api/articles/", // that have a URL that matches 'api/articles/'
      },
      {
        fixture: "multipleArticles.json",
      }
      // [] // and force the response to be: []
    ).as("ArticlesApi.getAll")
    cy.get(".p-datatable-tbody").contains("Testartikel")
  })
  // it("no articles given", () => {
  //   cy.intercept(
  //     {
  //       method: "GET", // Route all GET requests
  //       url: "/api/articles/", // that have a URL that matches '/users/*'
  //     },
  //     [] // and force the response to be: []
  //   ).as("ArticlesApi.getAll")
  //   cy.get(".p-datatable-tbody").contains("Keine Artikel vorhanden")
  // })
})
