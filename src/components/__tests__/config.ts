import { config, RouterLinkStub } from "@vue/test-utils"

import PrimeVue from "primevue/config"
import ToastService from "primevue/toastservice"

import Button from "primevue/button"
import DataTable from "primevue/datatable"
import Column from "primevue/column"
import ContextMenu from "primevue/contextmenu"
import Dialog from "primevue/dialog"
import InputText from "primevue/inputtext"
import SelectButton from "primevue/selectbutton"
import Dropdown from "primevue/dropdown"
import TabView from "primevue/tabview"
import TabPanel from "primevue/tabpanel"
import Editor from "primevue/editor"
import Tooltip from "primevue/tooltip"
import Toast from "primevue/toast"
import Menubar from "primevue/menubar"

config.global.components = {
  PToast: Toast,
  PDataTable: DataTable,
  PColumn: Column,
  PContextMenu: ContextMenu,
  PDialog: Dialog,
  PInputText: InputText,
  PDropdown: Dropdown,
  PSelectButton: SelectButton,
  PTabView: TabView,
  PTabPanel: TabPanel,
  PEditor: Editor,
  PButton: Button,
  PMenubar: Menubar,
}

config.global.directives = {
  tooltip: Tooltip,
}

config.global.plugins = [PrimeVue, ToastService]

config.global.stubs = {
  RouterLink: RouterLinkStub,
}
