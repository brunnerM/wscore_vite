export type Event = {
  evnNo: number
  evnArtNo: number
  evnDateStartday: string
  evnStartTime: string | null
  evnEndTime: string | null
}
