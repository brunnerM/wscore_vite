import { describe, it, expect } from "vitest"

import { mount } from "@vue/test-utils"
import ArticleFieldsDialog from "@/components/dialogs/ArticleFieldsDialog.vue"

import articleMockup from "@mock/article.json"

describe("ArticleFieldsDialog", () => {
  it("displayDialog is true", async () => {
    const wrapper = mount(ArticleFieldsDialog, {
      props: {
        modelValue: true,
        article: articleMockup,
      },
    })
    expect(wrapper.vm.displayDialog).toBe(true)
  })
  it("articleInDialog contains data from article", async () => {
    const wrapper = mount(ArticleFieldsDialog, {
      props: {
        modelValue: true,
        article: articleMockup,
      },
    })
    expect(wrapper.vm.articleInDialog.artName).toBe(articleMockup.artName)
  })
})
