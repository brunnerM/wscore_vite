export type Sale = {
  salNo: number
  salArtNo: number
  salPrcNo: number
  salType: number
  salDateFrom: string
  salDateTo: string
  salAdditionalProperties: any[]
}
