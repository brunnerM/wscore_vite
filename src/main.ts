import { createApp } from "vue"
import App from "./App.vue"
import router from "./router"
// import store from "./store"

// import custom plugins
import Websockets from "@/plugins/websockets"
import Primefaces from "@/plugins/primefaces"

// import global app styles
import "@/css/styles.scss"

const app = createApp(App)

// use plugins on app
app.use(router)
// app.use(store)
app.use(Websockets)
app.use(Primefaces)

app.mount("#app")
