export type Price = {
  prcNo: number
  // INFO: Java
  // prcArt wird zu prcArticle
  prcArt: number
  prcDateFrom: string
  prcDateTo: string
  prcPrice: number
  prcPercentage: number
}
