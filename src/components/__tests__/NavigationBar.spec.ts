import { describe, it, expect } from "vitest"

import { mount } from "@vue/test-utils"
import NavigationBar from "@/components/NavigationBar.vue"

describe("NavigationBar", () => {
  it("Main menu items are visible", async () => {
    const wrapper = mount(NavigationBar, {})
    expect(wrapper.text()).toContain("Tabellen")
    expect(wrapper.text()).toContain("Edit")
    expect(wrapper.text()).toContain("Users")
    expect(wrapper.text()).toContain("Events")
    expect(wrapper.text()).toContain("Quit")
  })
  it("search input is visible", async () => {
    const wrapper = mount(NavigationBar, {})
    expect(wrapper.get("input").attributes("placeholder")).toBe("Search")
  })
  it("submenu items are visible", async () => {
    const wrapper = mount(NavigationBar, {})
    expect(wrapper.get(".p-submenu-list").text()).toContain("Artikel")
    expect(wrapper.get(".p-submenu-list").text()).toContain("Events")
  })
})
