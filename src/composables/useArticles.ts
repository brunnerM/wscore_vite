import { ref } from "vue"
import { ArticlesApi } from "@/api/articles.api"
import type { Article } from "@/types/article"

// by convention, composable function names start with "use"
export function useArticles() {
  const articlesLoading = ref<boolean>(false)
  const articles = ref<Article[]>([])

  function loadAllArticles(): void {
    articlesLoading.value = true
    ArticlesApi.getAll().then(({ data }) => {
      articles.value = data
      articlesLoading.value = false
    })
  }

  // expose managed state as return value
  return { articlesLoading, articles, loadAllArticles }
}
