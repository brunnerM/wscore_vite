import type { Sale } from "@/types/sale"
import http from "@/api/http.api"

const sales_prefix = "/sales/"

export const SalesApi = {
  getAll() {
    return http.get<Sale[]>(sales_prefix)
  },
  create(data: Sale) {
    return http.post<Sale>(sales_prefix, data)
  },
  update(id: number, data: Sale) {
    return http.put<Sale>(sales_prefix + id + "/", data)
  },
}
