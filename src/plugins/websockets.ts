import Echo from "laravel-echo"
import Pusher from "pusher-js"
import type { App } from "vue"

export default {
  install(app: App, options: object) {
    const LaravelEcho = new Echo({
      broadcaster: "pusher",
      key: "123",
      wsHost: "localhost",
      wsPort: 6001,
      forceTLS: false,
      disableStats: true,
      namespace: "",
    })

    // Vue3 Composition API use
    app.provide("socket", LaravelEcho)

    // Options API use
    app.config.globalProperties.$socket = LaravelEcho
  },
}
