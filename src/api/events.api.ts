import type { Event } from "@/types/event"
import http from "@/api/http.api"

const events_prefix = "/events/"

export const EventsApi = {
  getAll() {
    return http.get<Event[]>(events_prefix)
  },
  create(data: Event) {
    return http.post<Event>(events_prefix, data)
  },
  update(id: number, data: Event) {
    return http.put<Event>(events_prefix + id + "/", data)
  },
}
