import type { Article } from "@/types/article"
import http from "@/api/http.api"

const articles_prefix = "articles/"

export const ArticlesApi = {
  getAll() {
    return http.get<Article[]>(articles_prefix)
  },
  get(id: number) {
    return http.get<Article>(articles_prefix + id + "/")
  },
  create(data: Article) {
    return http.post<Article>(articles_prefix, data)
  },
  update(id: number, data: Article) {
    return http.put<Article>(articles_prefix + id + "/", data)
  },
  delete(id: number) {
    return http.delete(articles_prefix + id + "/")
  },
  deleteAll() {
    return http.delete(articles_prefix)
  },
  findBy(key: string, value: string) {
    return http.get<Article[]>(`${articles_prefix}?${key}=${value}`)
  },
}
