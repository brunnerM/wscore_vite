import { ref } from "vue"
import { SalesApi } from "@/api/sales.api"
import type { Sale } from "@/types/sale"
import { useToast } from "primevue/usetoast"

// by convention, composable function names start with "use"
export function useSales() {
  const salesLoading = ref<boolean>(false)
  const saleSaving = ref<boolean>(false)
  const sales = ref<Sale[]>([])

  const toast = useToast()

  function loadAllSales(): void {
    salesLoading.value = true
    SalesApi.getAll().then(({ data }) => {
      sales.value = data
      salesLoading.value = false
    })
  }

  function saveSale(sale: Sale): void {
    saleSaving.value = true
    if (sale.salNo) {
      SalesApi.update(sale.salNo, sale).then(({ data }) => {
        saleSaving.value = false
      })
    } else {
      SalesApi.create(sale).then(({ data }) => {
        toast.add({
          severity: "success",
          summary: "Erfolgreich erstellt",
          detail: "Sale ID: " + data.salNo,
          life: 2000,
        })
        saleSaving.value = false
      })
    }
  }

  // expose managed state as return value
  return { salesLoading, sales, loadAllSales, saleSaving, saveSale }
}
