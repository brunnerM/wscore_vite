import { fileURLToPath, URL } from "url"

import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
      "@mock": fileURLToPath(new URL("./cypress/fixtures", import.meta.url)),
    },
  },
  server: {
    // port: 3000,
  },
  test: {
    setupFiles: ["./src/components/__tests__/config.ts"],
  },
})
