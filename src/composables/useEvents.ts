import { EventsApi } from "@/api/events.api"
import type { Event } from "@/types/event"
import { ref } from "vue"
import { useToast } from "primevue/usetoast"

export function useEvents(): any {
  const eventsLoading = ref<boolean>(true)
  const eventSaving = ref<boolean>(false)
  const events = ref<Event[]>([])
  const calendarEvents = ref<any[]>([])
  const calendarResources = ref<any[]>([])

  const toast = useToast()

  function loadAllEvents(): void {
    eventsLoading.value = true
    EventsApi.getAll().then(({ data }) => {
      events.value = data
      eventsLoading.value = false
    })
  }

  function saveEvent(event: Event): void {
    eventSaving.value = true
    if (event.evnNo) {
      EventsApi.update(event.evnNo, event).then(({ data }) => {
        eventSaving.value = false
      })
    } else {
      EventsApi.create(event).then(({ data }) => {
        toast.add({
          severity: "success",
          summary: "Erfolgreich erstellt",
          detail: "Event ID: " + data.evnNo,
          life: 2000,
        })
        eventSaving.value = false
      })
    }
  }

  return {
    eventsLoading,
    events,
    loadAllEvents,
    calendarEvents,
    calendarResources,
    saveEvent,
    eventSaving,
  }
}
